
# Développement JavaScript avancé et ubiquitaire, support de cours

Version 2020-03-01-03 © Aful https://aful.org/ 
distribué sous licence CC-BY-SA 4.0

Source : https://framagit.org/aful/developpement_javascript_avance_ubiquitaire-cours

---

## Écosystème, gouvernance et propriété

----

L’écosystème JavaScript n’est pas dépendant d’un acteur unique en position
dominante (comme c’est le cas pour l’écosystème Java où Java est la propriété
d’Oracle) :

* L’[Ecma Technical Committee 39 (TC39)](https://github.com/tc39)
  gère la norme ECMAScript qui définit le langage JavaScript et ses évolutions

* L’[OpenJS Foundation](https://openjsf.org/)
  (projet collaboratif de la Linux Foundation) maintient les projets
  JavaScript essentiels (Node.js, nvm, webpack, Electron, jQuery, etc.)

---

## Electron

----

[Electron](https://electronjs.org/) est très proche d’un navigateur web. Quand
utiliser Electron est-il pertinent ?

* Lorsqu’on veut accéder à des ressources uniquement disponibles depuis le
  système de l’utilisateur (ses fichiers – qu’il s’agisse de fichiers texte ou
  de musique, de vidéos, etc. — mais aussi le réseau local de son
  entreprise, etc.).
  Exemple le plus célèbre : l’[éditeur de texte Atom](https://atom.io/).

* Lorsqu’on veut être certain de pouvoir utiliser parfaitement une application
  web dépendant de technologies pointues (fonctionnement hors-ligne, etc.), par
  opposition aux applications web s’exécutant dans les navigateurs web suivant
  les principes d’«&nbsp;amélioration progressive » ou de « dégradation élégante »

* Lorsqu’on veut fournir une interface utilisateur épurée et/ou personnalisée
  n’affichant que les éléments graphiques de l’application développée, c’est à
  dire sans afficher tous les menus d’un navigateur web classique

---

## Exécution asynchrone

----

Situations avec beaucoup de latence : les interactions
événementielles et particulièrement tout ce qui est entrées-sorties.

La programmation asynchrone permet de gérer ces latences de manière efficace.
<!-- .element: class="fragment" -->

En JavaScript, et particulièrement dans Node.js, tous les appels entrées-sorties
sont à gérer de manière asynchrone.
<!-- .element: class="fragment" -->

Des méthodes sont aussi disponibles en version synchrone, mais à n'utiliser que
lorsqu'on ne peut pas faire autrement.
<!-- .element: class="fragment" -->

----

### Exécution asynchrone par callback

On peut gérer le code asynchrone avec des callbacks.

C'est la pratique par défaut dans Node.js, utilisée notamment pour le module `fs`.

```javascript
fs.readFile('/tmp/config.json', {encoding: 'utf8'}, function(err, res) {
    if (err) {
        console.error('Unable to read file')
    } else {
        try {
            const obj = JSON.parse(res)
            console.log(obj)
        } catch(err) {
            console.error('Invalid JSON in file:', err)
        }
    }
})
```
<!-- .element: class="fragment" -->

----

Mais cela devient pénible quand plusieurs actions asynchrones s’enchaînent
ou se déroulent en parallèle.

On peut alors se trouver dans une situation qu'on appelle le *callback hell*.

```javascript
// AVOID
asyncFunc1((err, res1) => {
  asyncFunc2(res1, (err, res2) => {
    asyncFunc3(res2, (err, res3) => {
      console.lor(res3)
    })
  })
})
```
<!-- .element: class="fragment" -->

----

### Exécution asynchrone par promesse

La meilleure solution → les promesses

En anglais on parle de "Promise" ou de "Thenable"

```javascript
// PREFER
asyncFuncPromise1()
  .then(asyncFuncPromise2)
  .then(asyncFuncPromise3)
  .then((result) => console.log(result))
  .catch((err) => console.error(err))
```
<!-- .element: class="fragment" -->

----

Node.js fournit la méthode `util.promisify` pour transformer n'importe quel
callback en promise.

```javascript
const util = require('util')
const readFile = util.promisify(require('fs').readFile)

readFile('/tmp/config.json', {encoding: 'utf8'})
    .then(JSON.parse)
    .then((obj) => {
        console.log(obj)
    })
    .catch((err) => {
        console.error('Some error occurred', err)
    })
```

&#9888; Ne pas installer le paquet NPM `util`. C'est le module
[`util`](https://nodejs.org/api/util.html) de Node.js qu'il faut utiliser.

----

Exemple de Promise la plus simple possible

```javascript
const promise1 = new Promise(function(resolve, reject) {
    resolve('Some result')
})
```

----

Exemple d’une méthode prenant des arguments et qui renvoie une Promise.

```javascript
function doSomething(arg1, arg2, arg3) {
    console.log('args:', arg1, arg2, arg3)
    return new Promise(function(resolve, reject) {
        resolve('Some result')
    })
})
```

En effet le plus utile n'est pas forcément d'écrire des promesses, mais plutôt
de *retourner* des promesses.

----

Exemple montrant comment gérer différemment les causes de rejet d'une
promesse dans le `catch` suivant le type d'Error émis.

```javascript
const util = require('util')
const readFile = util.promisify(require('fs').readFile)

readFile('/tmp/config.json', {encoding: 'utf8'})
    .then(JSON.parse)
    .then((obj) => {
        console.log(obj)
    })
    .catch((err) => {
        if (err instanceof SyntaxError) {
            console.error('Invalid JSON in file', err)
        } else {
            console.error('Unable to read file', err)
        }
    })
```

----

### async/await

ES2017 ajoute la notion de *async function* qui permet d'écrire du code
asynchrone quasiment comme du code synchrone. Cela devient encore plus lisible.

```javascript
// PREFER with asyn/await
async function() {
    try {
        const result1 = await asyncFuncPromise1()
        const result2 = await asyncFuncPromise2(result1)
        const result3 = await asyncFuncPromise3(result2)
        console.log(result3)
    } catch(err) {
        console.error(err))
    }
}
```
<!-- .element: class="fragment" -->

On ne peut utiliser await que dans le corps d'une function async.
<!-- .element: class="fragment" -->

----

### Promise dans les navigateurs

Les Promises ne sont pas supportées dans les navigateurs non-récents.

Aussi pour utiliser les Promises dans les navigateurs, il faut charger un
polyfill/shim qui ajoutera le support des Promises ou de certaines
fonctionnalités manquantes des Promises.

----

### Callback ou Promise ?

La plupart des paquets NPM avec un fonctionnement asynchrones fournissent une
API aussi bien utilisable avec des callbacks ou des Promises.
<!-- .element: class="fragment" -->

Le code avec des Promises est plus lisible et plus pratique. À privilégier.
<!-- .element: class="fragment" -->

Le code avec des Promises est un peu moins rapide qu'avec des callbacks. À prendre en compte
uniquement si il y a des problèmes de performances.
<!-- .element: class="fragment" -->

----

### Les références

* https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_promesses
* https://nodejs.org/api/util.html#util_util_promisify_original
* https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html

---

## Paquet NPM privé dans dépôt Git

----

On peut factoriser son code en créant des paquets NPM.
Mais on veut rarement rendre public tout son code ou tout le code d'une
entreprise en le publiant sur NPM.

Une solution est de mettre certains paquets NPM dans des dépôts Git privés.

Attention : Mettre le fichier `package.json` à la racine du dépôt Git privé.

Pour installer un paquet NPM privé d'une entreprise fictive :
```shellsession
$ npm i git+ssh://git@git.entreprise.fr:module1.git#v3.0.1
```

Exemple du paquet NPM depuis un dépôt Git (public pour tester la
manipulation) https://framagit.org/aful/git-npm-package :
```shellsession
$ npm i git+ssh://git@framagit.org:aful/git-npm-package.git
```

----

Formes possibles des URL de paquets NPM sur dépôt Git :

```
git://github.com/user/project.git#commit-ish
git+ssh://user@hostname:project.git#commit-ish
git+ssh://user@hostname/project.git#commit-ish
git+http://user@hostname/project/blah.git#commit-ish
git+https://user@hostname/project/blah.git#commit-ish
```

----

### Utilisation de ssh-agent

Pour pouvoir réaliser les installations de paquets NPM disponibles sur des
dépôts Git privé/ou à accès protégé, il faut utiliser `ssh-agent` qui va gérer
les demandes de la *passphrase* de votre clé privée chaque fois que nécessaire.

----

Pré-requis pour utiliser `ssh-agent` :

* disposer d’une bi-clé SSH

    On génère une nouvelle bi-clé SSH avec la commande suivante (bien garder les
    noms de fichiers proposés par défaut) :

    ```shellsession
    $ ssh-keygen -b 4096
    ```

    Sauf si vous savez parfaitement ce que vous faites, laissez le nom
    et le chemin proposé par défaut pour le fichier pour cette clé. Appuyez
    juste sur *Entrée*.

* que la clé publique de bi-clé SSH soit ajoutée à votre compte/profil Git/GitLab

----

### Utilisation de ssh-agent et ssh-add

La plupart des distributions GNU-Linux lance déjà automatiquement le programme
`ssh-agent` et vous n'aurez normalement pas à la faire, mais il faut le vérifier.

* Vérifier si le programme `ssh-agent` est déjà lancé avec la commande suivante :

    ```shellsession
    ps aux|grep ssh-agent
    ```

* Si `ssh-agent` n’est pas encore lancé, il faut alors le lancer :

    ```shellsession
    $ eval $(ssh-agent)
    ```

* Enfin exécuter la commande `ssh-add` pour charger la *passphrase* de votre clé
  privée :

    ```shellsession
    $ ssh-add
    ```

* On peut ensuite vérifier que la *passphrase* n'est plus demandée :

    ```shellsession
    $ ssh git@gitlab.com
    Welcome to GitLab, @user!
    Connection to gitlab.com closed.
    ```

---

## Curriculum vitae

Une fois le cours suivi et les TP réalisés, voici ce que vous pouvez mettre dans
vos CV :

* Langage : JavaScript (version ES8/ECMAScript 2017)

* Plateformes : Node.js, Electron

* Cadriciels (frameworks) : Express.js, Vue.js

* Outils : npm, browerify, webpack

---

FIN
