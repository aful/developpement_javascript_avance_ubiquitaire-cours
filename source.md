
# Développement JavaScript avancé et ubiquitaire, support de cours

Version 2024-11-13-01 © Aful https://aful.org/
distribué sous licence CC-BY-SA 4.0

Source : https://gitlab.com/aful/developpement_javascript_avance_ubiquitaire-cours

---

## Popularité de JavaScript

---

<!-- ![Modulecounts.com](image/modulecounts-2023.png) -->

<!-- <img src="image/modulecounts-2023.png" alt="Modulecounts.com" style="max-width: 100%; max-height: 100%;"/> -->
<img src="image/modulecounts-2023.png" alt="Modulecounts.com" style="width: 70%;"/>

http://www.modulecounts.com/

---

## Écosystème, gouvernance et propriété

----

L’écosystème JavaScript n’est pas dépendant d’un acteur unique en position
dominante (comme c’est le cas pour l’écosystème Java où Java est la propriété
d’Oracle) :

* L’[Ecma Technical Committee 39 (TC39)](https://github.com/tc39)
  gère la norme ECMAScript qui définit le langage JavaScript et ses évolutions

* L’[OpenJS Foundation](https://openjsf.org/)
  (projet collaboratif de la Linux Foundation) maintient les projets
  JavaScript essentiels (Node.js, nvm, webpack, Electron, jQuery, etc.)

---

## Sources et documents de référence

----

Documents de référence (API) :

* [La référence du langage JavaScript (tous les objets, toutes les fonctions, etc.)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
* [L'API Web — telle que disponible dans les navigateurs web et dans Node.js](https://developer.mozilla.org/en-US/docs/Web/API)
* [L'API de Node.js — qui fournit des fonctions supplémentaires à Node.js pour faire ce que ne fournit pas l'API Web](https://nodejs.org/api/)
* [L'API d'Express — qui fournit des fonctions supplémentaires pour faciliter le développement de serveur web](https://expressjs.com/en/4x/api.html)
* [REST API Tutorial](https://www.restapitutorial.com/)

---

## HTTPS

----

Chiffrement par Transport Layer Security (TLS); car Secure Sockets Layer (SSL)
déprécié

Tester qualité de protection d'un site web : https://www.ssllabs.com/ssltest/

Certaines fonctionnalités API Web soumises à mesures de sécurité (HTTPS) :
* https://developer.mozilla.org/fr/docs/Learn/JavaScript/Client-side_web_APIs/Introduction
* https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
* https://developer.mozilla.org/en-US/docs/Web/API/Push_API
* https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API

---

## CORS

[Cross-Origin Resource Sharing (CORS)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

---

## JSON API contre HTML

Points qui changent entre un serveur API REST et un serveur web servant du HTML :

* Chaque formulaire HTML faisant un `POST` doit être suivi d'un redirect vers
  une page web
* Chaque formulaire HTML doit être protégé contre les CSRF, alors qu'un serveur
  d'API REST est « naturellement » protégé

---

## Bonne gestion du cache (Cache-Control)

----

Toujours spécifier des entêtes de gestion de cache :
[`Cache-Control`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control),
[`Vary`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Vary)

Exemples avec Node.js ou Express :

```
res.setHeader('Cache-Control', 'no-cache');

res.setHeader('Cache-Control', 'private, max-age=600');
res.setHeader('Vary', 'Cookie');
```

Exemples avec Koa :

```
ctx.set('Cache-Control', `private, must-revalidate, max-age=600`)
ctx.set('Vary', 'Authorization')
```

---

## REST API

----

Pour les API REST avec workflow simple : CRUD

[REST API Tutorial](https://www.restapitutorial.com/)

----

Pour les API REST avec workflow complexe : verbes et concepts abstraits

[REST API Design - Resource Modeling - Escaping CRUD](https://www.thoughtworks.com/insights/blog/rest-api-design-resource-modeling)

---

## Exécution asynchrone

----

Situations avec beaucoup de latence : les interactions
événementielles et particulièrement tout ce qui est entrées-sorties.

La programmation asynchrone permet de gérer ces latences de manière efficace.
<!-- .element: class="fragment" -->

En JavaScript, et particulièrement dans Node.js, tous les appels entrées-sorties
sont gérés par défaut de manière asynchrone.
<!-- .element: class="fragment" -->

Des functions/méthodes sont aussi disponibles en version synchrone, mais ce sont des
exceptions et pas la règle, il ne faut les utiliser que lorsqu'on ne peut
pas faire autrement.
<!-- .element: class="fragment" -->

----

### Exécution asynchrone par callback

On peut gérer le code asynchrone avec des callbacks.

```javascript
const {readFile} = require('fs')

fs.readFile('/tmp/config.json', 'utf8', function(err, data) {
    if (err) {
        console.error('Unable to read file')
    } else {
        try {
            const obj = JSON.parse(data)
            console.log(obj)
        } catch(err) {
            console.error('Invalid JSON in file:', err)
        }
    }
})
```
<!-- .element: class="fragment" -->

----

Mais cela devient pénible quand plusieurs actions asynchrones s’enchaînent
ou se déroulent en parallèle

On peut alors se trouver dans une situation d’appels en cascade se décalant de
plus en plus vers la droite (*callback hell*)

```javascript
// AVOID
asyncFunc1((err1, result1) => {
  asyncFunc2(result1, (err2, result2) => {
    asyncFunc3(result2, (err3, result3) => {
      console.log(result3)
    })
  })
})
```
<!-- .element: class="fragment" -->

----

### Exécution asynchrone par promesse

La meilleure solution → les promesses

En anglais on parle de "Promise" ou de "Thenable"

```javascript
// PREFER
asyncFuncPromise1()
  .then(asyncFuncPromise2)
  .then(asyncFuncPromise3)
  .then((result) => console.log(result))
  .catch((err) => console.error(err))
```
<!-- .element: class="fragment" -->

----

```javascript
const {readFile} = require('fs').promises

readFile('/tmp/config.json', 'utf8')
    .then(JSON.parse)
    .then((obj) => {
        console.log(obj)
    })
    .catch((err) => {
        console.error('Some error occurred', err)
    })
```

----

Exemple montrant comment gérer différemment les causes de rejet d'une
promesse dans le `catch` suivant le type d'Error émis.

```javascript
const {readFile} = require('fs').promises

readFile('/tmp/config.json', 'utf8')
    .then(JSON.parse)
    .then((obj) => {
        console.log(obj)
    })
    .then((obj) => {
        return doSomeOtherAction(obj)
    })
    .catch((err) => {
        if (err instanceof SyntaxError) {
            console.error('Invalid JSON in file', err)
        } else if (err instanceof ValidationError) {
            console.error('File contains wrong data', err)
        } else {
            console.error('Unknown error', err)
        }
    })
```

----

Création d’une Promise

```javascript
const promise1 = new Promise(function(resolve, reject) {
    if (everything_ok) {
        resolve('Some result')
    } else {
        reject(new Error('Something wrong happened!'))
    }
})
```

----

Exemple d’une méthode prenant des arguments et qui renvoie une Promise.

```javascript
function doSomething(arg1, arg2, arg3) {
    console.log('args:', arg1, arg2, arg3)
    return new Promise(function(resolve, reject) {
        resolve('Some result')
    })
})
```

En effet le plus utile n'est pas forcément d'écrire des promesses, mais plutôt
de *retourner* des promesses.

----

### async/await

ES2017 ajoute la notion de *async function* qui permet d'écrire du code
asynchrone quasiment comme du code synchrone. Cela devient encore plus lisible.

```javascript
// PREFER with async/await
async function() {
    try {
        const result1 = await asyncFuncPromise1()
        const result2 = await asyncFuncPromise2(result1)
        const result3 = await asyncFuncPromise3(result2)
        console.log(result3)
    } catch(err) {
        console.error(err)
    }
}
```
<!-- .element: class="fragment" -->

----

### Exécution de promesses en parallèle avec Promise.all()

```javascript
const promise1 = Promise.resolve(3);
const promise2 = 42;
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values);
});
// Expected output: Array [3, 42, "foo"]
```

Et il existe de nombreuses possibilités pour créer des ordonnancements spécifiques d'exécution de promesses.

----

### Callback ou Promise ?

La plupart des paquets NPM avec un fonctionnement asynchrone fournissent une
API aussi bien utilisable avec des Promises qu’avec des callbacks.
<!-- .element: class="fragment" -->

Le code avec des Promises est plus lisible, plus pratique, plus maintenable. À
privilégier.
<!-- .element: class="fragment" -->

----

### Les références

* https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_promesses
* https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html

---

## Modules ESM et CommonJS

2 formats de modules coexistent : l'ancien format *CommonJS* et le nouveau
format *ECMAScript modules* (ESM) normalisé pour le langage JavaScript

Le format ESM fait tout ce que l'ancien format CommonJS fait, tout en faisant
des choses supplémentaires
<!-- .element: class="fragment" -->

Utiliser le format ESM aussi bien côté client que côté serveur
<!-- .element: class="fragment" -->

La plupart des outils savent gérer les 2 formats
<!-- .element: class="fragment" -->

---

## Paquet NPM privé dans dépôt Git

----

Formes possibles des URL de paquets NPM sur dépôt Git :

```
git://github.com/user/project.git#commit-ish
git+ssh://user@hostname:project.git#commit-ish
git+ssh://user@hostname/project.git#commit-ish
git+http://user@hostname/project/blah.git#commit-ish
git+https://user@hostname/project/blah.git#commit-ish
```

----

### Utilisation de ssh-agent

Pour pouvoir réaliser les installations de paquets NPM disponibles sur des
dépôts Git privé/ou à accès protégé, il faut utiliser `ssh-agent` qui va gérer
les demandes de la *passphrase* de votre clé privée chaque fois que nécessaire.

----

Pré-requis pour utiliser `ssh-agent` :

* disposer d’une bi-clé SSH

    On génère une nouvelle bi-clé SSH avec la commande suivante (bien garder les
    noms de fichiers proposés par défaut) :

    ```shellsession
    ssh-keygen -t ed25519 -C "nom.prenom@example.net"
    ```

    Sauf si vous savez parfaitement ce que vous faites, laissez le nom
    et le chemin proposé par défaut pour le fichier pour cette clé. Appuyez
    juste sur *Entrée*.

* que la clé publique de bi-clé SSH soit ajoutée à votre compte/profil Git/GitLab

----

### Utilisation de ssh-agent et ssh-add

La plupart des distributions GNU-Linux lance déjà automatiquement le programme
`ssh-agent` et vous n'aurez normalement pas à la faire, mais il faut le vérifier.

* Vérifier si le programme `ssh-agent` est déjà lancé avec la commande suivante :

    ```shellsession
    ps aux|grep ssh-agent
    ```

* Si `ssh-agent` n’est pas encore lancé, il faut alors le lancer :

    ```shellsession
    $ eval $(ssh-agent)
    ```

* Enfin exécuter la commande `ssh-add` pour charger la *passphrase* de votre clé
  privée :

    ```shellsession
    $ ssh-add
    ```

* On peut ensuite vérifier que la *passphrase* n'est plus demandée :

    ```shellsession
    $ ssh git@gitlab.com
    Welcome to GitLab, @user!
    Connection to gitlab.com closed.
    ```

---

## Développement du paquet NPM privé

----

On peut factoriser son code en créant des paquets NPM.

On peut publier des paquets NPM sous forme publique et aussi sous forme privée.

La publication privée correspond à de nombreux cas d’utilisation d’organisations/d’entreprises, même si ces structures peuvent publier une partie plus ou moins importante de leur code source sous forme de logiciels libres.

Une solution pour cela est de publier des paquets NPM dans la *Package Registry* d’une instance GitLab auto-hébergée (self-hosted) en accès restreint.

Attention : Le `name` du `package.json` doit être de la forme `@my-org/package-name` ou  `@my-user/package-name`.

Pour installer un paquet NPM privé d'une organisation/d’entreprise fictive :

```shellsession
set -a; . .env; set +a
npm i -S @my-user/package-name
```

---

## Electron

----

[Electron](https://electronjs.org/) est très proche d’un navigateur web. Quand
utiliser Electron est-il pertinent ?

* Lorsqu’on veut accéder à des ressources uniquement disponibles depuis le
  système de l’utilisateur (ses fichiers – qu’il s’agisse de fichiers texte ou
  de musique, de vidéos, etc. — mais aussi le réseau local de son
  entreprise, etc.).
  Exemple le plus célèbre : l’[éditeur de texte Atom](https://atom.io/).

* Lorsqu’on veut être certain de pouvoir utiliser parfaitement une application
  web dépendant de technologies pointues (fonctionnement hors-ligne, etc.), par
  opposition aux applications web s’exécutant dans les navigateurs web suivant
  les principes d’«&nbsp;amélioration progressive » ou de « dégradation élégante »

* Lorsqu’on veut fournir une interface utilisateur épurée et/ou personnalisée
  n’affichant que les éléments graphiques de l’application développée, c’est à
  dire sans afficher tous les menus d’un navigateur web classique

---

## Curriculum vitae

Une fois le cours suivi et les TP réalisés, voici ce que vous pouvez mettre dans
vos CV :

* Langage : JavaScript (version ES8/ECMAScript 2017)

* Plateformes : Node.js, Electron

* Cadriciels (frameworks) : Express.js, Vue.js

* Outils : npm, webpack, browserify

---

## FIN
