Développement JavaScript avancé et ubiquitaire, support de cours
================================================================

Support de cours/formation Développement JavaScript avancé et ubiquitaire
(ES2015, Node.js, NPM, browserify, Express, Electron, etc.)


Utilisation
-----------

    $ npm ci

et ensuite ouvrir le fichier `index.html` avec un navigateur web avec
le support de JavaScript activé.

C'est déployé et testable sur
https://aful.frama.io/developpement_javascript_avance_ubiquitaire-cours


Documentation
-------------

https://github.com/hakimel/reveal.js/
